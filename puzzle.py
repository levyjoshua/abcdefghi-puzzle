"""
Find answers to

 AB  
x C
---
 DE
+FG
---
 HI
"""
from math import factorial
from itertools import ifilter, permutations

def stream_perms(n, k=None):
  """
  stream k-element permutations from an 
  n-element set
  """
  if k is None:
    k = n
  # for n=5, k=2 limit = 5!/(5-2)!
  n_k_fac = factorial(n-k)
  limit = factorial(n) / n_k_fac
  bases = [factorial(n-1-j) for j in range(n)]
  for x in xrange(limit):
    vals = range(1, n+1)
    term = x * n_k_fac
    for y in xrange(n-1):
      idx = term / bases[y]
      if idx:
        term = term % bases[y]
        vals[y], vals[y + idx] = vals[y + idx], vals[y]
    yield vals[:k]

def stream_nots(*seen):
  """
  generate numbers 1-9 but not in seen
  """
  ss = set(seen)
  for j in range(1,10):
    if j not in ss:
      yield j

def find_answers():
  for c in stream_nots(1,5,8,9):
    for a in stream_nots(c,*range(5,10)):
      for b in stream_nots(c, a, 1, 5):
        ab = (10 * a) + b
        abc = frozenset([a,b,c])
        de = ab * c
        if (de < 100):
          d = de / 10
          e = de % 10
          if (d == e) or (e == 0) or (d in abc) or (e in abc):
            continue # next b
          for f in stream_nots(c, a, b, d, e):
            for g in stream_nots(c, a, b, d, e, f):
              fg = (10 * f) + g
              abcdefg = frozenset([a,b,c,d,e,f,g])
              hi = de + fg
              if (hi < 100):
                h = hi / 10
                i = hi % 10
                if (h == i) or (i == 0) or (h in abcdefg) or (i in abcdefg):
                  continue
                yield(ab, c, de, fg, hi)

def check_permutation(x):
  ab = 10 * x[0] + x[1]
  c = x[2]
  de = 10 * x[3] + x[4]
  if ((ab * c) == de):
    fg = 10 * x[5] + x[6]
    hi = 10 * x[7] + x[8]
    return ((de + fg) == hi)
                    
def find_all_answers():
  return list(find_answers())

def find_from_perms():
  return ifilter(check_permutation, stream_perms(9))

def find_all_from_perms():
  return list(find_from_perms())


def check_5perm(x):
  seen = [(j == 0) for j in xrange(10)]
  for j in x:
    seen[j] = True
  ab = 10 * x[0] + x[1]
  c = x[2]
  de = ab * c
  if de < 100:
    d, e = de/10, de%10
    if seen[d] or seen[e] or (d == e):
      return False
    seen[d] = seen[e] = True
    fg = 10 * x[3] + x[4]
    hi = de + fg
    if hi < 100:
      h, i = hi/10, hi % 10
      if seen[h] or seen[i] or (h == i):
        return False
      return True
  return False

def find_from_5perms():
  return ifilter(check_5perm, stream_perms(9,5))

def find_all_from_5perms():
  return list(find_from_5perms())

def find_from_it_5perms():
  return ifilter(check_5perm, permutations(range(1,10),5))

def find_all_from_it_5perms():
  return list(find_from_it_5perms())
